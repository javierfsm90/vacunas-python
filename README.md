# **INSTALACION**

1. Abrir una consola y clonar el proyecto (
    `git clone https://gitlab.com/javierfsm90/vacunas-python.git`
) 
2. Ingresar a la carpeta descargada
> `cd vacunas-python`

2. A continuación, en la consola abriremos una Shell de MySQL y crearemos la base de datos llamada `vacuna` 
>
>`mysql -u root -p`
>
>`create database vacunas;`
>
>Salir de la Shell pulsando `CLRT+C `
3. Una vez en la linea de comandos normal importaremos la base de datos
>
>`mysql -u root -p vacunas < scriptSQL.sql`

4. Luego instalar los requerientos del proyecto y ejecutar el programa
>`pip install -r requirements.txt`
>
>`python vacuna.py`
5. Abrir en el navegador el siguiente link http://127.0.0.1:5000/
