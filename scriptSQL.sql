-- MariaDB dump 10.17  Distrib 10.5.2-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: vacunas
-- ------------------------------------------------------
-- Server version	10.5.2-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `paciente`
--

DROP TABLE IF EXISTS `paciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paciente` (
  `paciente_id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_nombre` varchar(30) NOT NULL,
  `paciente_apellido` varchar(30) NOT NULL,
  `paciente_rut` varchar(12) NOT NULL,
  `paciente_fecha_nac` date NOT NULL,
  PRIMARY KEY (`paciente_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paciente`
--

LOCK TABLES `paciente` WRITE;
/*!40000 ALTER TABLE `paciente` DISABLE KEYS */;
INSERT INTO `paciente` VALUES (1,'Pedro','Aguilar','24551202-3','1997-03-31'),(2,'Diego ','Barrientos','24198304-8','2001-09-21'),(3,'Eloisa','Miranda ','3008995-2','1970-03-17'),(4,'Schlomit','Venegas','17786263-0','1991-05-10'),(5,'Yeril','Caceres','20588806-3','1998-12-18'),(6,'Jipsy','Feldman','21200643-2','2011-11-11'),(7,'Jorge','Araneda','9870568-6','1969-09-16');
/*!40000 ALTER TABLE `paciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paciente_recibe_vacuna`
--

DROP TABLE IF EXISTS `paciente_recibe_vacuna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paciente_recibe_vacuna` (
  `recibe_id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) NOT NULL,
  `vacuna_id` int(11) NOT NULL,
  `recibe_fecha` date NOT NULL,
  PRIMARY KEY (`recibe_id`),
  KEY `paciente_recibe_vacuna_paciente_id_foreign` (`paciente_id`),
  KEY `paciente_recibe_vacuna_vacuna_id_foreign` (`vacuna_id`),
  CONSTRAINT `paciente_recibe_vacuna_paciente_id_foreign` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`paciente_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `paciente_recibe_vacuna_vacuna_id_foreign` FOREIGN KEY (`vacuna_id`) REFERENCES `vacuna` (`vacuna_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paciente_recibe_vacuna`
--

LOCK TABLES `paciente_recibe_vacuna` WRITE;
/*!40000 ALTER TABLE `paciente_recibe_vacuna` DISABLE KEYS */;
INSERT INTO `paciente_recibe_vacuna` VALUES (1,2,3,'2020-04-02'),(2,3,6,'2016-01-11'),(3,6,1,'2020-04-02'),(4,1,7,'2017-11-08'),(5,4,5,'2018-04-15'),(6,5,4,'2020-04-02'),(7,7,2,'2002-03-16'),(8,2,1,'2015-02-15'),(9,3,4,'2020-04-02'),(10,7,4,'2020-04-02'),(11,5,3,'2013-10-19'),(12,4,2,'2020-04-02'),(13,1,3,'2020-04-02'),(14,7,6,'2018-12-13'),(15,5,7,'2010-09-03'),(16,6,5,'2003-12-30');
/*!40000 ALTER TABLE `paciente_recibe_vacuna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacuna`
--

DROP TABLE IF EXISTS `vacuna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacuna` (
  `vacuna_id` int(11) NOT NULL AUTO_INCREMENT,
  `vacuna_nombre_enfermedad` varchar(100) NOT NULL,
  `vacuna_fecha_reg` date NOT NULL,
  PRIMARY KEY (`vacuna_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacuna`
--

LOCK TABLES `vacuna` WRITE;
/*!40000 ALTER TABLE `vacuna` DISABLE KEYS */;
INSERT INTO `vacuna` VALUES (1,'Hepatitis B ','2019-04-02'),(2,'Hexavalente','2020-04-02'),(3,'Neumocócica conjugada','2020-03-14'),(4,'Hepatitis A','2018-04-01'),(5,'Varicela','2020-04-02'),(6,'Fiebre Amarilla','2020-03-11'),(7,'Neumocócica polisacárida','2017-04-02');
/*!40000 ALTER TABLE `vacuna` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-02 18:58:26
