import requests
import json
from datetime import date, datetime
from flask import Flask, render_template, jsonify, request, redirect, url_for
from flaskext.mysql import MySQL
import pymysql
import pymysql.cursors


app = Flask(__name__)
app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_DB"] = "vacunas"

mysql = MySQL(app)
mysql.connect_args["autocommit"] = True
mysql.connect_args["cursorclass"] = pymysql.cursors.DictCursor

# <==== PACIENTES ====>

@app.route('/')
def pacientes():
    cursor = mysql.get_db().cursor()

    sql = "SELECT * FROM paciente"
    cursor.execute(sql)

    pacientes_ = cursor.fetchall()

    for paciente in pacientes_:
        paciente['paciente_fecha_nac'] = paciente['paciente_fecha_nac'].strftime("%d-%m-%Y")

    return render_template("index.html", pacientes = pacientes_)

@app.route('/pacientesJson')
def pacientes_json():
	cursor = mysql.get_db().cursor()
	cursor.execute("SELECT * FROM paciente")
	return jsonify(cursor.fetchall())

@app.route('/paciente/add')
def addPaciente():
    return render_template('addPaciente.html')


@app.route('/insertPaciente', methods=['POST'])
def insertPaciente():

    if request.method == "POST":
        nombre = request.form['inputNombre']
        apellido = request.form['inputApellido']
        date = request.form['inputDate']
        rut = request.form['rut']
        cursor = mysql.get_db().cursor()
        cursor.execute(
            "INSERT INTO paciente (paciente_nombre, paciente_apellido, paciente_fecha_nac, paciente_rut) VALUES (%s, %s, %s, %s)", (nombre, apellido, date, rut))
        # return render_template('index.html')
        return redirect(url_for('pacientes'))

# <==== VACUNAS ====>

@app.route('/vacunas')
def vacunas():
    cursor = mysql.get_db().cursor()

    sql = "SELECT * FROM vacuna"
    cursor.execute(sql)

    vacunas_ = cursor.fetchall()

    for vacuna in vacunas_:
        vacuna['vacuna_fecha_reg'] = vacuna['vacuna_fecha_reg'].strftime("%d-%m-%Y")

    return render_template("vacunas.html", vacunas = vacunas_)

@app.route('/vacunasJson')
def vacunas_json():
	cursor = mysql.get_db().cursor()
	cursor.execute("SELECT * FROM vacunas")
	return jsonify(cursor.fetchall())

@app.route('/vacuna/add')
def addVacuna():
    return render_template('addVacuna.html')


@app.route('/insertVacuna', methods=['POST'])
def insertVacuna():

    if request.method == "POST":
        nombre = request.form['inputNombre']
        fecha  = date.today()

        cursor = mysql.get_db().cursor()
        cursor.execute(
            "INSERT INTO vacuna (vacuna_nombre_enfermedad, vacuna_fecha_reg) VALUES (%s, %s)", (nombre, fecha))
        return redirect(url_for('vacunas'))



# <==== VACUNAR PACIENTE ====>

@app.route('/paciente/<int:id>/addVacuna')
def pacienteIdVacuna(id):
    cursor = mysql.get_db().cursor()

    sql_paciente = "SELECT * FROM paciente WHERE paciente_id=%s"
    cursor.execute(sql_paciente,id)
    paciente_ = cursor.fetchone()
    paciente_['paciente_fecha_nac'] = paciente_['paciente_fecha_nac'].strftime("%d-%m-%Y")
    

    sql_vacunas = "SELECT * FROM vacuna"
    cursor.execute(sql_vacunas)
    vacunas_ = cursor.fetchall()

    return render_template("vacunarPaciente.html", paciente = paciente_,vacunas = vacunas_)



@app.route('/insetRecibeVacuna', methods=['POST'])
def insetRecibeVacuna():
    if request.method == "POST":
        paciente_id = request.form['inputPacienteId']
        vacuna_id = request.form['inputVacunaId']
        fecha  = date.today()

        cursor = mysql.get_db().cursor()
        cursor.execute(
            "INSERT INTO paciente_recibe_vacuna (paciente_id, vacuna_id,recibe_fecha) VALUES (%s, %s, %s)", (paciente_id, vacuna_id,fecha))
        return redirect(url_for('pacientes'))

@app.route('/paciente/<int:id>/vacunas')
def viewPacienteVacunas(id):
    # print("id =",id)
    cursor = mysql.get_db().cursor()

    sql_vacunas = "SELECT p.paciente_nombre,p.paciente_apellido,v.vacuna_nombre_enfermedad,r.recibe_fecha FROM  paciente_recibe_vacuna r INNER JOIN  paciente p INNER JOIN  vacuna v ON r.paciente_id = p.paciente_id and r.vacuna_id=v.vacuna_id WHERE p.paciente_id=%s;"
    cursor.execute(sql_vacunas,id)
    vacunas_ = cursor.fetchall()
    for vacuna in vacunas_:
        vacuna['recibe_fecha'] = vacuna['recibe_fecha'].strftime("%d-%m-%Y")
    

    sql_paciente = "SELECT paciente_nombre,paciente_apellido FROM paciente WHERE paciente_id=%s"
    cursor.execute(sql_paciente,id)
    paciente_ = cursor.fetchone()
    return render_template("viewPacienteVacunas.html",vacunas = vacunas_, paciente = paciente_)


@app.route('/vacuna/<int:id>/pacientes')
def viewVacunaPacientes(id):
    # print("id =",id)
    cursor = mysql.get_db().cursor()

    sql_pacientes = "SELECT p.paciente_nombre,p.paciente_apellido,p.paciente_rut,r.recibe_fecha FROM  paciente_recibe_vacuna r INNER JOIN  paciente p INNER JOIN  vacuna v ON r.paciente_id = p.paciente_id and r.vacuna_id=v.vacuna_id WHERE v.vacuna_id=%s;"
    cursor.execute(sql_pacientes,id)
    pacientes_ = cursor.fetchall()
    for paciente in pacientes_:
        paciente['recibe_fecha'] = paciente['recibe_fecha'].strftime("%d-%m-%Y")

    sql_vacuna = "SELECT vacuna_nombre_enfermedad FROM vacuna WHERE vacuna_id=%s"
    cursor.execute(sql_vacuna,id)
    vacuna_ = cursor.fetchone()
    return render_template("viewVacunaPacientes.html",pacientes = pacientes_, vacuna = vacuna_)


# <==== FORMATO ====>

@app.route('/formato/<int:id>/addVacuna')
def formato(id):
    cursor = mysql.get_db().cursor()

    sql_paciente = "SELECT * FROM paciente WHERE paciente_id=%s"
    cursor.execute(sql_paciente,id)
    paciente_ = cursor.fetchone()

    sql_vacunas = "SELECT * FROM vacuna"
    cursor.execute(sql_vacunas)
    vacunas_ = cursor.fetchall()

    return render_template("zformato.html", paciente = paciente_,vacunas = vacunas_)



if __name__ == "__main__":
    app.run(debug=True)
